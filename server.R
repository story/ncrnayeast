library(shiny)

chr.match <- readRDS('chr_df.RDS')
chr.options <- as.character(chr.match$ensembl)
datasets <- list.files('www/plots')

# list("E-MTAB-1246"="Transcription profiling by tiling array of combinations of Rrp6p and Dis3p catalytic mutants to investigate extensive degradation of RNA precursors by the exosome in yeast",
#      "E-MTAB-1250"="Transcription profiling by tiling array of yeast undergoing a series of carbon source shifts to discern how Set3C affects gene expression",
#      "E-MTAB-1686"="Transcription profiling by tiling array of Saccharomyces cerevisiae exposed to osmotic shock to investigate lncRNAs induced specifically by the Hog1 stress-activated protein kinase (SAPK)",
#      "E-MTAB-2241"="Transcription profiling by tiling array of wild-type or reb1-deleted budding yeast strains",
#      "E-MTAB-3642"="Transcriptional profiling by array of Npl3 knockout S. cerevisiae to study transcription readthrough",
#      "E-MTAB-4268"="Transcription profiling of Set2 deletion in yeast to investigate how H3K36 methylation affects mRNA transcription during carbon source shifts",
#      "E-TABM-590"="Transcription profiling of wild type yeast grown with ethanol, glucose and galactose and the deletion mutant of Rrp6 to identify transcription start and end positions",
#      "E-TABM-901"="Tiling path analysis of Saccharomyces cerevisiae reveals a genome-wide role for the SAS-I histone acetyltransferase in transcription elongation and suppression of cryptic initiation",
#      "E-TABM-936"="Transcription profiling by tiling array of yeast ssu72, rrp6, and rco1 mutants"
#      )

# Define server logic required to generate and plot a random distribution
shinyServer(function(input, output, session){
  output$dataSelect <- renderUI({
    selectInput( "dataset", "Dataset", choices = datasets)
  })
  output$textExp <- renderText({
    exps <- list("E-MTAB-1246"="Transcription profiling by tiling array of combinations of Rrp6p and Dis3p catalytic mutants to investigate extensive degradation of RNA precursors by the exosome in yeast",
         "E-MTAB-1250"="Transcription profiling by tiling array of yeast undergoing a series of carbon source shifts to discern how Set3C affects gene expression",
         "E-MTAB-1686"="Transcription profiling by tiling array of Saccharomyces cerevisiae exposed to osmotic shock to investigate lncRNAs induced specifically by the Hog1 stress-activated protein kinase (SAPK)",
         "E-MTAB-2241"="Transcription profiling by tiling array of wild-type or reb1-deleted budding yeast strains",
         "E-MTAB-3642"="Transcriptional profiling by array of Npl3 knockout S. cerevisiae to study transcription readthrough",
         "E-MTAB-4268"="Transcription profiling of Set2 deletion in yeast to investigate how H3K36 methylation affects mRNA transcription during carbon source shifts",
         "E-TABM-590"="Transcription profiling of wild type yeast grown with ethanol, glucose and galactose and the deletion mutant of Rrp6 to identify transcription start and end positions",
         "E-TABM-901"="Tiling path analysis of Saccharomyces cerevisiae reveals a genome-wide role for the SAS-I histone acetyltransferase in transcription elongation and suppression of cryptic initiation",
         "E-TABM-936"="Transcription profiling by tiling array of yeast ssu72, rrp6, and rco1 mutants"
    )
    paste0('<p><em>',exps[[input$dataset]],'</em><p>')
  })
  output$chromSelect <- renderUI({
    selectInput( "chrom", "Chromosome", choices = chr.options, selected = "chrI")
  })
  output$genomicPositionSelect <- renderUI({
    sliderInput( "gpos", "Genomic Position:", min = 0, max = chr.match[(input$chrom),]$length, value = 1, step = 10000)
  })
  # pos <- reactive({
  #   min( max( input$windowsize + 1, input$gpos ), (chr.match[(input$chrom),]$length - input$windowsize - 1) )
  # })
  # #pos <- 647000
  # rangeplot <- reactive({
  #   c( max( pos() - input$windowsize, 0 ), min( pos() + input$windowsize, chr.match[(input$chrom),]$length ))
  # })
  observeEvent(input$gpos, {
    x <- input$gpos
    if (x == 0 | is.na(x)){
      updateNumericInput(session, inputId = "gpos", value = 1)
    }
    if(as.numeric(x) > 1e4*round(chr.match[(input$chrom),]$length/1e4)){
      updateNumericInput(session, inputId = "gpos", value = 1e4*round(chr.match[(input$chrom),]$length/1e4))
    }
  })
  output$thePlot <- renderImage({
    loc <- input$gpos
    if(as.numeric(input$gpos) > 1){
      loc <- input$gpos+1
    }
    filename <- normalizePath(file.path('www','plots',input$dataset,paste0(input$chrom,'_',loc,'.png')))
    ##tags$iframe(src = filename, width = "100%", height = "100%")
    list(src = filename)
  }, deleteFile = FALSE)
  # output$thePlot <- renderUI({
  #   print(input$gpos)
  #   print(input$dataset)
  #   print(input$chrom)
  #   if(input$gpos > 1){
  #     loc <- input$gpos+1
  #   }else{
  #     loc <- input$gpos
  #   }
  #   filename <- normalizePath(file.path('plots',input$dataset,paste0(input$chrom,'_',loc,'.png')))
  #   print(filename)
  #   tags$iframe(src = filename, width = "100%", height = "100%")
  # })
})
