FROM rocker/shiny:4.0.3

COPY www/ /srv/shiny-server/www/
COPY ./packages.R packages.R
COPY chr_df.RDS /srv/shiny-server/chr_df.RDS

ADD server.R /srv/shiny-server/
ADD ui.R /srv/shiny-server/

RUN Rscript packages.R
